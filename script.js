const focusInput = document.getElementById("input");
const error = document.getElementById("error");
const price = document.getElementById("total-price");
const span = document.querySelector(".current-price");
const clearAll = document.querySelector(".price-dlt")

focusInput.addEventListener("focus", function() {
    focusInput.classList.add('border-green');
});

focusInput.addEventListener("blur", function() {
    focusInput.classList.remove('border-green');
});

focusInput.addEventListener('input', showResultFocus);
function showResultFocus (value){
    focusInput.addEventListener("focus", function() {
        if (value.target.value < 0) {
            focusInput.classList.remove('border-red');
            price.classList.remove('price-block');
            error.innerHTML = "";
            focusInput.value = "";
        } else if(value.target.value > 0) {
            price.classList.add('price-block');
            span.textContent = `Текущая цена: ${value.target.value}`;
            clearAll.classList.remove('hide');
        };
})};

focusInput.addEventListener('input', showResultBlur);
function showResultBlur (value){
    focusInput.addEventListener("blur", function() {
        if (value.target.value < 0) {
            focusInput.classList.add('border-red');
            error.innerHTML = 'Please enter correct price';
            error.classList.add('text-warning');
        } else if(value.target.value > 0) {
            focusInput.classList.remove('border-red');
            error.innerHTML = "";
            price.classList.add('price-block');
            span.textContent = `Текущая цена: ${value.target.value}`;
            clearAll.classList.remove('hide');
        };
})};

clearAll.addEventListener('click', function(){
    price.classList.remove('price-block');
    focusInput.value = "";
    span.textContent = "";
});


